#include "MessagesSender.h"

mutex con_lock_m;
mutex d_lock_m; 
condition_variable con_info; 


MessageSender::MessageSender()
{
	this->loged_in_users = new vector<string>;
	this->messages = new queue<string>;
}

MessageSender::~MessageSender()
{
	if (!this->loged_in_users && !this->messages)
	{
		delete this->loged_in_users;
		delete this->messages;
	}
}

void MessageSender::menu()
{
	int op = 0;

	while (op != 4)
	{
		system("cls");
		cout << "message sender prog" <<endl
			<<"1. to sign in" <<endl
			<<"2. to sign out" << endl
			<<"3. to see all connected users" << endl
			<<"4. to exit" << endl;
		cin >> op;
		system("cls");
		switch (op)
		{
		case 1: this->signin(); break;
		case 2: this->signout(); break;
		case 3: this->print_users(); break;
		case 4:
			cout << "goodbye :-)" << endl;
			system("pause");
			break;
		default: cout << "bad input!" << endl; break;
		}
	}
}

vector<string> MessageSender::getUsers()
{
	return *this->loged_in_users;
}

void MessageSender::signin()
{
	string userName;
	vector<string> users = getUsers();
	cout << "username pls: ";
	cin >> userName;
	if (user_exists(userName))
	{
		cout << "this username already exists..." << endl;
		getchar();
	}
	else
	{
		unique_lock<mutex> conLock(con_lock_m);
		this->loged_in_users->push_back(userName);
	}
}

void MessageSender::signout()
{
	string userName;
	cout << "enter username to signout: ";
	cin >> userName;
	if (user_exists(userName))
	{
		unique_lock<mutex> conLock(con_lock_m);


		(*this->loged_in_users).erase(remove((*this->loged_in_users).begin(), 
		(*this->loged_in_users).end(), userName),
		(*this->loged_in_users).end());

		cout << "user signed out" << endl;
		getchar();
	}
	else
	{
		cout << "no user found" << endl;

		getchar();
	}
	getchar();
}

void MessageSender::print_users()
{
	int count = 0;
	for (auto name : *this->loged_in_users)
	{
		cout << ++count << ". " << name << endl;
	}
	system("pause");
}

void MessageSender::read_from_file()
{
	string line;
	ifstream dataFile;
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(6000));
		dataFile.open("data.txt");
		if (dataFile){
			while (getline(dataFile, line)){
				unique_lock<mutex> dataLock(d_lock_m);
				this->messages->push(line);
				dataLock.unlock();
				con_info.notify_one();
			}
		}
		else
		{
			cout << "cant open data.txt";
		}
		dataFile.close();
		dataFile.open("data.txt", ofstream::out | ofstream::trunc);
		dataFile.close();
	}

}

void MessageSender::send_msg()
{
	ofstream messageFile;
	string message;
	while (1)
	{
		unique_lock<mutex> dataLock(d_lock_m);
		con_info.wait(dataLock, [=]() {return !(*this->messages).empty(); });
		messageFile.open("output.txt", ofstream::out | ofstream::app);
		if (messageFile)
		{
			message = this->messages->front();
			this->messages->pop();
			dataLock.unlock();

			for (auto user : *this->loged_in_users)
			{
				messageFile << user << ": " << message << endl;
			}
			messageFile.close();
		}
		else cout << "Unable to open output file";
	}
}

bool MessageSender::user_exists(string name)
{
	for (auto user : *this->loged_in_users) if (user.data() == name) return true;

	return false;
}