#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stdio.h>
#include <fstream>
#include <queue>
#include <Windows.h>
#include <mutex>
#include <condition_variable>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::queue;
using std::mutex;
using std::condition_variable;
using std::unique_lock;
using std::ifstream;
using std::ofstream;



class MessageSender
{
	vector<string>* loged_in_users;
	queue<string>* messages;
	vector<string> getUsers();
	void signin();
	void signout();
	void print_users();
	bool user_exists(string name);

public:
	MessageSender();
	~MessageSender();
	void menu();
	void read_from_file();
	void send_msg();
	
};