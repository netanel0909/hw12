#include <iostream>
#include "MessagesSender.h"
#include <thread>

using std::thread;

int main()
{
	MessageSender m;
	thread t1(&MessageSender::menu, m);
	thread t2(&MessageSender::read_from_file, m);
	thread t3(&MessageSender::send_msg, m);
	t1.join();
	t2.detach();
	t3.detach();

	return 0;
}