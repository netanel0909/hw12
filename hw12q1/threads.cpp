#include "threads.h"
#include <mutex>

std::mutex m;

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}


void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join();
}


void getPrimes(int begin, int end, vector<int>& primes)
{
	int i = begin;
	while (i != end)
	{
		if (is_prime(i))
		{
			primes.push_back(i);
		}

		i++;
	}
}

bool is_prime(int num)
{
	int i = (int)sqrt(num) + 1;
	while (i != 1)
	{
		if (num % i == 0) return false;
		i--;
	}
	return true;
}


void printVector(std::vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	std::clock_t start;
	double duration;

	vector<int> primes;

	start = std::clock();

	thread t1(getPrimes, begin, end, ref(primes));
	t1.join();

	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;

	printVector(primes);
	cout << duration << " seconds" << endl;

	return primes;
}


void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	for (int i = begin; i < end; i++)
	{
		m.lock();
		if(is_prime(i)) file << i << endl;
		m.unlock();
	}
	file << endl;
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	vector<thread> _threads;
	ofstream file;
	file.open(filePath);
	int diff = (end - begin) / N;
	int _end = begin + diff;
	int _begin = begin;
	const clock_t begin_time = clock();
	for (int i = 0; i < N; i++)
	{
		_threads.push_back(thread(writePrimesToFile, _begin, _end, ref(file)));
		_begin = _end;
		_end += diff;

	}
	for (int i = 0; i < N; i++)
	{
		_threads[i].join();
	}
	cout << float(clock() - begin_time) / CLOCKS_PER_SEC << " seconds" << endl;
	file.close();
}
