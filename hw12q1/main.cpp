#include "threads.h"



int main()
{
	call_I_Love_Threads();

	//vector<int> primes1 = callGetPrimes(0, 74); //this line can run very quikly and is beter for checking the program
	vector<int> primes1 = callGetPrimes(0, 1000);
	vector<int> primes2 = callGetPrimes(0, 100000);
	vector<int> primes3 = callGetPrimes(0, 1000000);

	//callWritePrimesMultipleThreads(0, 74, "f1.txt", 5); //this line can run very quikly and is beter for checking the program
	callWritePrimesMultipleThreads(0, 1000, "f1.txt", 5);
	callWritePrimesMultipleThreads(0, 100000, "f2.txt", 5);
	callWritePrimesMultipleThreads(0, 1000000, "f3.txt", 5);

	system("pause");
	return 0;
}
